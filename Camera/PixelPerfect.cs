﻿using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class PixelPerfect : MonoBehaviour {
		public float pixelPerUnit = 100f;
		public void Awake(){
			camera.orthographicSize = (Screen.height / pixelPerUnit / 2.0f); // 100f is the PixelPerUnit that you have set on your sprite. Default is 100.
		}
	}
}