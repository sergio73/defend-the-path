﻿using UnityEngine;
using System.Collections;

namespace DefendThePath{
	[System.Serializable]
	public class Main : Scene{
		public int quantityMainButtons;
		public int sizeMainButtons;
		public int quantitySubButtons;
		public int sizeSubButtons;

		private int mainMargin;
		private int actualMainMargin;
		private int subMargin;
		private int actualSubMargin;


		protected override void Draw(OnClick onClick){
			#region MainButtons
			//Calcule the margin between buttons
			this.mainMargin = (Screen.width - (this.sizeMainButtons * this.quantityMainButtons))/(this.quantityMainButtons + 1);
			//Reset the actual margin
			this.actualMainMargin = this.mainMargin;

			if(GUI.Button(this.AddElement(this.actualMainMargin, 50, this.sizeMainButtons, this.sizeMainButtons, false), "Levels"))
				onClick(typeof(Levels));
			this.actualMainMargin += this.mainMargin + this.sizeMainButtons;

			if(GUI.Button(this.AddElement(this.actualMainMargin, 50, this.sizeMainButtons, this.sizeMainButtons, false), "Wiki"))
				onClick(typeof(Wiki));
			#endregion

			this.AddSpaceDown(105);

			#region subButtons
			//Calcule the margin between buttons
			this.subMargin = (Screen.width - (this.sizeSubButtons * this.quantitySubButtons))/(this.quantitySubButtons + 1);
			//Reset the actual margin
			this.actualSubMargin = this.subMargin;

			if(GUI.Button(this.AddElement(this.actualSubMargin, 0, this.sizeSubButtons, this.sizeSubButtons, false), "Settings"))
				onClick(typeof(Settings));
			this.actualSubMargin += this.subMargin + this.sizeSubButtons;
			
			if(GUI.Button(this.AddElement(this.actualSubMargin, 0, this.sizeSubButtons, this.sizeSubButtons, false), "Stats"))
				onClick(typeof(Stats));
			this.actualSubMargin += this.subMargin + this.sizeSubButtons;
			
			if(GUI.Button(this.AddElement(this.actualSubMargin, 0, this.sizeSubButtons, this.sizeSubButtons, false), "About me"))
				onClick(typeof(AboutMe));
			#endregion
		}

		//Hide home button
		protected override void DrawAwake (OnClick onClick){
			this.DrawTitle();
		}
	}
}