using UnityEngine;
using System.Collections;

namespace DefendThePath{
	[System.Serializable]
	public class Stats : Scene{
		[Range(0,50)]
		public int marginX = 0;
		[Range(0,50)]
		public int marginY = 0;

		GUIStyle labelStyle;
		void Start(){
			this.labelStyle = new GUIStyle(this.GetSkin().GetStyle("LabelStyle"));
		}

		private int shots = 0;
		private int enemyDead = 0;
		private int score = 0;
		private int money = 0;
		private int moneySpent = 0;
		private int wins = 0;
		private int loses = 0;

		protected override void Draw (OnClick onClick){
			GUI.Label(this.AddElement(this.marginX,this.marginY,100,20), "Shots: " + this.shots, this.labelStyle);
			GUI.Label(this.AddElement(this.marginX,this.marginY,100,20), "Enemies deads: " + this.enemyDead, this.labelStyle);
			GUI.Label(this.AddElement(this.marginX,this.marginY,100,20), "Totally score: " + this.score, this.labelStyle);
			GUI.Label(this.AddElement(this.marginX,this.marginY,100,20), "Totally money: " + this.money, this.labelStyle);
			GUI.Label(this.AddElement(this.marginX,this.marginY,100,20), "Totally money spent: " + this.moneySpent, this.labelStyle);
			GUI.Label(this.AddElement(this.marginX,this.marginY,100,20), "Wins: " + this.wins, this.labelStyle);
			GUI.Label(this.AddElement(this.marginX,this.marginY,100,20), "Loses: " + this.loses, this.labelStyle);
		
			this.AddSpaceDown(105);
			if(GUI.Button(this.AlignToRight(this.AddElement(0,0,100,100), 5), "Restart"))
				this.Reset();
		}

		public override void OnEnter(){
			this.shots = PlayerPrefs.GetInt(PlayerPrefsName.Shots);
			this.enemyDead = PlayerPrefs.GetInt(PlayerPrefsName.EnemyDead);
			this.score = PlayerPrefs.GetInt(PlayerPrefsName.Score);
			this.money = PlayerPrefs.GetInt(PlayerPrefsName.Money);
			this.moneySpent = PlayerPrefs.GetInt(PlayerPrefsName.MoneySpent);
			this.wins = PlayerPrefs.GetInt(PlayerPrefsName.Wins);
			this.loses = PlayerPrefs.GetInt(PlayerPrefsName.Loses);
		}

		private void Reset(){
			PlayerPrefs.SetInt(PlayerPrefsName.Shots, 0);
			PlayerPrefs.SetInt(PlayerPrefsName.EnemyDead, 0);
			PlayerPrefs.SetInt(PlayerPrefsName.Score, 0);
			PlayerPrefs.SetInt(PlayerPrefsName.Money, 0);
			PlayerPrefs.SetInt(PlayerPrefsName.MoneySpent, 0);
			PlayerPrefs.SetInt(PlayerPrefsName.Wins, 0);
			PlayerPrefs.SetInt(PlayerPrefsName.Loses, 0);

			this.OnEnter();
		}
	}
}