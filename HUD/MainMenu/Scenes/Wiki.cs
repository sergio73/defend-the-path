using UnityEngine;
using System.Collections;

namespace DefendThePath{
	[System.Serializable]
	public class Wiki : Scene{

		[Range(0,200)]
		public int sizeButtons = 0;
		[Range(1,5)]
		public int qntyButtons = 1;
		[Range(0,200)]
		public int marginY = 1;

		protected override void Draw (OnClick onClick){
			this.CalculateMargin(this.sizeButtons, this.qntyButtons);

			if(GUI.Button(this.AddElement(this.marginElementsActual,this.marginY,this.sizeButtons,this.sizeButtons, false),"Player"))
				onClick(typeof(WikiPlayer));
			this.AddElementMargin();

			if(GUI.Button(this.AddElement(this.marginElementsActual,this.marginY,this.sizeButtons,this.sizeButtons, false),"Enemy"))
				onClick(typeof(WikiEnemy));
			this.AddElementMargin();

		}
	}
}