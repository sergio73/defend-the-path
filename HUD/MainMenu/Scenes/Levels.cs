﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DefendThePath{
	[System.Serializable]
	public class Levels : Scene{
		public List<Map> maps = new List<Map>();

		[Range(0,200)]
		public int marginTop;

		[Range(1,5)]
		public int buttonsPerLine;
		[Range(1,20)]
		public int buttonsOnScreen;
		[Range(0,200)]
		public int buttonsSize;
		[Range(0,100)]
		public int marginLine;
		[Range(0,100)]
		public int marginInfoX;
		[Range(0,100)]
		public int marginInfoDesX;
		[Range(0,100)]
		public int marginInfoY;

		public Texture2D starFull;
		public Texture2D starVoid;
		private static Texture2D starFullStat;
		private static Texture2D starVoidStat;

		private GUIStyle labelStyle;

		private int buttonsMargin;
		private int buttonsMarginActual;

		private int currentGroup = 0;

		private void Start(){
			this.labelStyle = this.GetSkin().GetStyle("LabelStyle");

			starFullStat = this.starFull;
			starVoidStat = this.starVoid;
		}
		protected override void Draw(OnClick onClick){
			this.ResetMargin();

			int i;
			for(i = 0; i < this.buttonsOnScreen; i++){
				if((i + this.currentGroup) < this.maps.Count){
					//New line
					if(i % this.buttonsPerLine == 0 && i != 0){
						this.AddSpace(this.buttonsSize + this.marginLine);
						this.ResetMargin();
					}
					if(GUI.Button(this.AddElement(this.buttonsMarginActual, this.marginTop, this.buttonsSize, this.buttonsSize, false), this.maps[i + this.currentGroup].mapImage))
						this.maps[i + this.currentGroup].LoadLevel();
					GUI.Label(this.AddElement(this.buttonsMarginActual + this.buttonsSize + this.marginInfoX, this.marginTop + this.marginInfoY, this.buttonsSize, this.buttonsSize, false), "Stars: ", this.labelStyle);
					this.maps[i + this.currentGroup].DrawStars(this.AddElement(this.buttonsMarginActual + this.buttonsSize + this.marginInfoX + this.marginInfoDesX, this.marginTop + this.marginInfoY, 17, 17, false));

					GUI.Label(this.AddElement(this.buttonsMarginActual + this.buttonsSize + this.marginInfoX, this.marginTop + this.marginInfoY + 20, this.buttonsSize, this.buttonsSize, false), "Score: " + this.maps[i+this.currentGroup].score, this.labelStyle);
					this.buttonsMarginActual += this.buttonsMargin + this.buttonsSize;
				}
			}

			this.AddSpaceDown(105);

			if(GUI.Button(this.AlignToRight(this.AddElement(0,0,100,100, false),5), ">"))
				this.NextGroup();
			if(GUI.Button(this.AlignToRight(this.AddElement(0,0,100,100, false), 110), "<"))
				this.PreviousGroup();
		}

		private void ResetMargin(){
			//Reset margins
			//Calcule the margin between buttons
			this.buttonsMargin = (Screen.width - (this.buttonsSize * this.buttonsPerLine))/(this.buttonsPerLine + 1);
			//Reset the actual margin
			this.buttonsMarginActual = this.buttonsMargin;
		}

		private void NextGroup(){
			if((this.currentGroup + this.buttonsOnScreen) >= this.maps.Count)
				return;

			this.currentGroup += this.buttonsOnScreen;
		}
		private void PreviousGroup(){
			if((this.currentGroup - this.buttonsOnScreen) < 0)
				return;

			this.currentGroup -= this.buttonsOnScreen;
		}
		private void ReloadMaps(){
			foreach(Map map in this.maps){
				map.LoadProperties();
			}
		}


		public override void OnEnter(){
			this.ReloadMaps();
		}
		public static Texture2D GetStarVoid(){
			return starVoidStat;
		}
		public static Texture2D GetStarFull(){
			return starFullStat;
		}
	}
}