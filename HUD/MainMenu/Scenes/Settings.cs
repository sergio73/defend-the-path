﻿using UnityEngine;
using System.Collections;


namespace DefendThePath{
	[System.Serializable]
	public class Settings : Scene{
		[Range(0,200)]
		public int marginY;
		[Range(0,400)]
		public int maringDescription;
		[Range(0,100)]
		public int buttonSize;
		[Range(0,50)]
		public int labelSize;


		private GUIStyle labelStyleSlider;
		private GUIStyle labelStyleDefinition;

		private void Start(){
			this.labelStyleSlider = new GUIStyle(this.GetSkin().GetStyle("LabelStyle"));
			this.labelStyleDefinition = new GUIStyle(this.GetSkin().GetStyle("LabelStyle"));

			this.labelStyleSlider.alignment = TextAnchor.MiddleCenter;
			this.labelStyleDefinition.alignment = TextAnchor.MiddleLeft;
			this.labelStyleDefinition.fontSize = this.labelSize;
		}

		private float sSoundGeneral = 0;
		private float sSoundMusic = 0;
		private float sSoundEffects = 0;
		protected override void Draw(OnClick onClick){
			if(this.saving)
				return;

			GUI.Label(this.AddElement(maringDescription,10,150,20), "General sound", this.labelStyleDefinition);
			this.sSoundGeneral = GUI.HorizontalSlider(this.CenterElement(this.AddElement(0,20,500,30,false)), this.sSoundGeneral, 0, 100);
			GUI.Label(this.CenterElement(this.AddElement(0,20,500,30,false)), Mathf.RoundToInt(this.sSoundGeneral).ToString(), this.labelStyleSlider);
			PlayerPrefs.SetFloat(PlayerPrefsName.SoundGeneral, this.sSoundGeneral / 100);
			this.AddSpace(marginY);

			GUI.Label(this.AddElement(maringDescription,10,150,30), "Music sound", this.labelStyleDefinition);
			this.sSoundMusic = GUI.HorizontalSlider(this.CenterElement(this.AddElement(0,20,500,30,false)), this.sSoundMusic, 0, 100);
			GUI.Label(this.CenterElement(this.AddElement(0,20,500,30, false)), Mathf.RoundToInt(this.sSoundMusic).ToString(), this.labelStyleSlider);
			PlayerPrefs.SetFloat(PlayerPrefsName.SoundMusic, this.sSoundMusic / 100);
			this.AddSpace(marginY);

			GUI.Label(this.AddElement(maringDescription,10,150,30), "Effects sound", this.labelStyleDefinition);
			this.sSoundEffects = GUI.HorizontalSlider(this.CenterElement(this.AddElement(0,20,500,30,false)), this.sSoundEffects, 0, 100);
			PlayerPrefs.SetFloat(PlayerPrefsName.SoundEffects, this.sSoundEffects / 100);
			GUI.Label(this.CenterElement(this.AddElement(0,20,500,30, false)), Mathf.RoundToInt(this.sSoundEffects).ToString(), this.labelStyleSlider);


			this.AddSpaceDown(this.buttonSize + 5);
			if(GUI.Button(this.AlignToRight(this.AddElement(0,0,this.buttonSize, this.buttonSize), 5),"Ok")){
				PlayerPrefs.SetFloat(PlayerPrefsName.SoundGeneral, this.sSoundGeneral / 100);
				PlayerPrefs.SetFloat(PlayerPrefsName.SoundMusic, this.sSoundMusic / 100);
				PlayerPrefs.SetFloat(PlayerPrefsName.SoundEffects, this.sSoundEffects / 100);

				onClick(typeof(Main));
			}
		}
		private bool saving;

		//Data enter
		float sGeneralEnter;
		float sMusicEnter;
		float sEffectsEnter;
		public override void OnEnter(){
			this.sGeneralEnter = PlayerPrefs.GetFloat(PlayerPrefsName.SoundGeneral, 0.5F) * 100;
			this.sMusicEnter = PlayerPrefs.GetFloat(PlayerPrefsName.SoundMusic, 0.5F) * 100;
			this.sEffectsEnter = PlayerPrefs.GetFloat(PlayerPrefsName.SoundEffects, 0.5F) * 100;

			this.sSoundGeneral = this.sGeneralEnter;
			this.sSoundMusic = this.sMusicEnter;
			this.sSoundEffects = this.sEffectsEnter;

			this.saving = false;
		}

		//Exit write default data
		protected override void DrawHomeButton(OnClick onClick){
			if(GUI.Button(new Rect(5,Screen.height - 105,100,100), "Home")){
				PlayerPrefs.SetFloat(PlayerPrefsName.SoundGeneral, this.sGeneralEnter / 100);
				PlayerPrefs.SetFloat(PlayerPrefsName.SoundMusic, this.sMusicEnter / 100);
				PlayerPrefs.SetFloat(PlayerPrefsName.SoundEffects, this.sEffectsEnter / 100);

				this.saving = true;

				onClick(typeof(Main));
			}
		}

	}
}