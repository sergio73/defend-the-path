using UnityEngine;
using System.Collections;

namespace DefendThePath{
	[System.Serializable]
	public class AboutMe : Scene{
		[Multiline]
		public string description_ES;
		[Multiline]
		public string description_EN;
		private string descriptionShow;
		private bool descriptionToggle = false;

		[Range(0,960)]
		public int marginDescriptionX = 0;
		[Range(0,600)]
		public int marginDescriptionY = 0;
		[Range(0,960)]
		public int marginDescriptionWidth = 0;
		[Range(0,600)]
		public int marginDescriptionHeight = 0;

		[Range(0,100)]
		public int marginSeparator = 0;
		[Range(0,100)]
		public int widthSeparator = 0;


		[Range(0,200)]
		public int buttonsSize = 0;


		public Texture2D twitter;
		public Texture2D email;
		public Texture2D git;

		private GUIStyle boxStyle;

		protected override void Draw (OnClick onClick){
			GUI.Box(this.AddElement(this.marginDescriptionX, this.marginDescriptionY, this.marginDescriptionWidth, this.marginDescriptionHeight, false), this.descriptionShow, this.boxStyle);

			GUI.Box(this.AddElement(this.marginDescriptionX + this.marginDescriptionWidth + this.marginSeparator, this.marginDescriptionY, this.widthSeparator, this.marginDescriptionHeight, false), GUIContent.none);

			if(GUI.Button(this.AddElement(this.marginDescriptionX + this.marginDescriptionWidth + this.marginSeparator * 2 + this.widthSeparator, this.marginDescriptionY, this.buttonsSize, this.buttonsSize, true), this.email))
				Application.OpenURL("mailto:admin@sergio73.es");
			this.AddSpace(this.marginSeparator);
			if(GUI.Button(this.AddElement(this.marginDescriptionX + this.marginDescriptionWidth + this.marginSeparator * 2 + this.widthSeparator, this.marginDescriptionY, this.buttonsSize, this.buttonsSize, true), this.twitter))
				Application.OpenURL("https://twitter.com/SergioDelgado73");

			this.AddSpace(this.marginSeparator);
			if(GUI.Button(this.AddElement(this.marginDescriptionX + this.marginDescriptionWidth + this.marginSeparator * 2 + this.widthSeparator, this.marginDescriptionY, this.buttonsSize, this.buttonsSize, true), this.git))
				Application.OpenURL("https://bitbucket.org/sergio73/defend-the-path");

			this.AddSpaceDown(105);
			if(GUI.Button(this.AlignToRight(this.AddElement(0,0,150,100), 5), "Español/English")){
				if(this.descriptionToggle){
					this.descriptionShow = this.description_EN;
					this.descriptionToggle = false;
				}else{
					this.descriptionShow = this.description_ES;
					this.descriptionToggle = true;
				}
			}

		}

		private void Start(){
			this.descriptionShow = this.description_EN;

			this.boxStyle = new GUIStyle(this.GetSkin().GetStyle("Box"));
			this.boxStyle.wordWrap = true;
		}
	}
}