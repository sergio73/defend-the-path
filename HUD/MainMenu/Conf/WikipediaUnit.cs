using System;
using UnityEngine;

namespace DefendThePath{
	[System.Serializable]
	public class WikipediaUnit{
		public GameObject unit;
		[Multiline]
		public string description;

		[HideInInspector]
		public PlayerUnit player;
		[HideInInspector]
		public EnemyUnit enemy;

		[HideInInspector]
		public string nameUnit;
		[HideInInspector]
		public Texture2D image;

		public void Initialize(){
			if(this.unit.GetComponent<PlayerUnit>()){
				this.player = this.unit.GetComponent<PlayerUnit>();
				this.LoadPlayer();
			}else{
				this.enemy = this.unit.GetComponent<EnemyUnit>();
				this.LoadEnemy();
			}
		}

		private void LoadPlayer(){
			this.nameUnit = this.player.nameUnit;
			this.image = this.player.GetTextureHighRes();
		}

		private void LoadEnemy(){
			this.nameUnit = this.enemy.nameEnemy;
			this.image = this.enemy.GetTextureHighRes();
		}
	}
}

