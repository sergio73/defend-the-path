﻿using UnityEngine;
using System.Collections;

namespace DefendThePath{
	[System.Serializable]
	public class Map{
		public string mapName;
		public Texture2D mapImage;

		[Range(0,3)]
		public int stars = 0;
		public int score = 0;

		public void LoadLevel(){
			Application.LoadLevel(mapName);
		}

		private Texture2D starFull, starVoid;
		public Map(Texture2D starF, Texture2D starV){
			this.starFull = starF;
			this.starVoid = starV;
		}

		public void DrawStars(Rect pos){
			for(int i = 0; i < 3; i++){
				if(i != 0)
					pos.x += 20;

				if(i < this.stars)
					GUI.Box(pos, Levels.GetStarFull());
				else
					GUI.Box(pos, Levels.GetStarVoid());
			}
		}


		public void DrawStars(Rect pos, float margin){
			for(int i = 0; i < 3; i++){
				if(i != 0)
					pos.x += pos.width + margin;
				
				if(i < this.stars)
					GUI.Box(pos, this.starFull);
				else
					GUI.Box(pos, this.starVoid);
			}
		}
		public void LoadProperties(){
			this.stars = PlayerPrefs.GetInt(this.mapName + "_" + "stars");
			this.score = PlayerPrefs.GetInt(this.mapName + "_" + "score");
		}
		public void LoadPropertiesThisMap(){
			this.stars = PlayerPrefs.GetInt(Application.loadedLevelName + "_" + "stars");
			this.score = PlayerPrefs.GetInt(Application.loadedLevelName + "_" + "score");
		}
	}
}
