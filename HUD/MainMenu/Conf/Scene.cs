using UnityEngine;
using System.Collections;
using System;

namespace DefendThePath{
	[System.Serializable]
	public abstract class Scene : MonoBehaviour{
		public string SceneName;

		protected static GUISkin skinHUD;
		private GUIStyle style = new GUIStyle();

		void Awake(){
			if(skinHUD == null){
				skinHUD = this.GetComponent<MenuController>().skinHUD;
			}
			this.style = skinHUD.GetStyle("SceneStyle");
		}
		#region Simplificating HUD
		private float yPosLastElement = 0;

		/// <summary>
		/// Increases Y position;
		/// </summary>
		/// <returns>The amount that Y has increased.</returns>
		/// <param name="amount">Amount.</param>
		private float IncreasePosLastElement(float amount){
			this.yPosLastElement += amount;
			return amount;
		}
		/// <summary>
		/// Adds an element increasing the Y position.
		/// </summary>
		/// <returns>The element.</returns>
		/// <param name="xPos">X position.</param>
		/// <param name="yPos">Y position.</param>
		/// <param name="width">Width.</param>
		/// <param name="height">Height.</param>
		/// <param name="floating">Floating.</param>
		protected Rect AddElement(float xPos, float yPos, float width, float height, bool floating){
			if(floating)
				return new Rect(xPos, yPos + this.yPosLastElement, width , this.IncreasePosLastElement(height));
			else
				return new Rect(xPos, yPos + this.yPosLastElement, width , height);
		}
		/// <summary>
		/// Default parameter to AddElement method.
		/// </summary>
		/// <returns>The element.</returns>
		/// <param name="xPos">X position.</param>
		/// <param name="yPos">Y position.</param>
		/// <param name="width">Width.</param>
		/// <param name="height">Height.</param>
		protected Rect AddElement(float xPos, float yPos, float width, float height){
			return AddElement(xPos, yPos, width, height, true);
		}

		/// <summary>
		/// Adds a space.
		/// </summary>
		/// <param name="height">Height.</param>
		protected void AddSpace(float height){
			IncreasePosLastElement(height);
		}

		/// <summary>
		/// Substract to Screen the specfied height
		/// </summary>
		/// <param name="height">Height.</param>
		protected void AddSpaceDown(float height){
			this.ResetPosLastElement(0);
			IncreasePosLastElement(Screen.height - height);
		}

		protected void SubtractSpace(float height){
			IncreasePosLastElement(-height);
		}

		/// <summary>
		/// Resets the position last element.
		/// </summary>
		/// <param name="amount">Amount.</param>
		protected void ResetPosLastElement(int amount){
			this.yPosLastElement = amount;
		}
		protected void ResetPosLastElement(){
			this.ResetPosLastElement(0);
		}

		protected Rect AlignToRight(Rect box, float margin){
			box.x = Screen.width - box.width - margin;
			return box;
		}
		protected Rect AlignToRight(Rect box){
			return AlignToRight(box, 0);
		}

		protected Rect CenterElement(Rect box){
			box.x = (Screen.width - box.width) / 2;
			return box;
		}


		int marginElements;
		int sizeElements;
		protected int marginElementsActual;
		/// <summary>
		/// Calculate the margin between buttons.
		/// </summary>
		/// <returns>The margin between buttons.</returns>
		/// <param name="sizeButtons">Size buttons.</param>
		/// <param name="quantityButtons">Quantity buttons.</param>
		protected int CalculateMargin(int sizeElements, int quantityElements){
			this.marginElements = (Screen.width - (sizeElements * quantityElements))/(quantityElements + 1);
			this.marginElementsActual = this.marginElements;

			this.sizeElements = sizeElements;
			return this.marginElements;
		}
		protected int AddElementMargin(){
			this.marginElementsActual += this.marginElements + this.sizeElements;
			return this.marginElementsActual;
		}
		#endregion



		//Delegate for clicks. When button is pressed this delegated is called like OnClick(typeof(Levels))
		public delegate void OnClick(Type scene);

		#region AbstractMethods

		//Execute first, can be replaced if the scene does'n require show any of this things
		protected virtual void DrawAwake(OnClick onClick){
			this.DrawTitle();
			this.DrawHomeButton(onClick);
		}

		protected virtual void DrawTitle(){
			this.ResetPosLastElement(100);
			GUI.Label(this.AddElement(5,5,300,50), this.SceneName, this.style); 
		}
		protected virtual void DrawHomeButton(OnClick onClick){
			if(GUI.Button(new Rect(5,Screen.height - 105,100,100), "Home"))
				onClick(typeof(Main));
		}

		protected abstract void Draw(OnClick onClick);
		#endregion

		protected GUISkin GetSkin(){
			return skinHUD;
		}


		#region Public
		public virtual void OnEnter(){}
		public virtual void OnExit(){}
		/// <summary>
		/// Render the scene.
		/// </summary>
		/// <param name="onClick">On click.</param>
		public void Render(OnClick onClick){
			this.DrawAwake(onClick);
			this.Draw(onClick);
		}
		#endregion
	}
}
