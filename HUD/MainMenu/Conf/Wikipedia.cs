using System;
using UnityEngine;
using System.Collections.Generic;

namespace DefendThePath{
	public class Wikipedia : Scene{
		public bool enemy = false;
		public bool player = false;

		[Range(0,400)]
		public int widthList = 0;
		[Range(0,600)]
		public int heightList = 0;
		[Range(0,50)]
		public int heightButton = 0;
		[Range(0,50)]
		public int marginProperties = 0;

		[Range(0,150)]
		public int imageSize = 0;

		public List<WikipediaUnit> units = new List<WikipediaUnit>();

		void Start(){
			if(this.enemy && this.player)
				Debug.LogError("Can not be true enemy and player at same time");
			if(!this.enemy && !this.player)
				Debug.LogError("Can not be false enemy and player at same time");

			foreach(WikipediaUnit unit in units){
				unit.Initialize();
			}
			boxCenter = new GUIStyle(this.GetSkin().GetStyle("Box"));
			boxCenter.alignment = TextAnchor.MiddleCenter;

			labelStyle = new GUIStyle(this.GetSkin().GetStyle("LabelStyle"));
			labelStyle.fontSize = 16;

			labelDescription = new GUIStyle(this.GetSkin().GetStyle("Box"));;
			labelDescription.wordWrap = true;
		}
		GUIStyle boxCenter;
		GUIStyle labelStyle;
		GUIStyle labelDescription;

		WikipediaUnit unitSelected;
		int margin;
		protected override void Draw (OnClick onClick){
			//Show names
			GUI.Box(this.AddElement(5,0,this.widthList,this.heightList, false), GUIContent.none);
			foreach(WikipediaUnit unit in units){
				if(GUI.Button(this.AddElement(10,5,this.widthList - 10, this.heightButton), unit.nameUnit))
					this.unitSelected = unit;
				this.AddSpace(5);
			}

			//Description
			this.ResetPosLastElement(150);
			margin = this.widthList + 10;
			GUI.Box(this.AddElement(margin,0, Screen.width - this.widthList - 15,this.heightList, false), GUIContent.none);

			if(unitSelected != null){
				GUI.Box(this.AddElement(margin + 5, 5, this.imageSize, this.imageSize, false), this.unitSelected.image, this.boxCenter);
				GUI.Box(this.AddElement(margin + 10 + this.imageSize, 5, Screen.width - margin - 20 - this.imageSize, this.imageSize, true), this.unitSelected.description, labelDescription);

				this.AddSpace(this.marginProperties);

				if(this.player)
					this.PlayerProperties();
				else
					this.EnemyProperties();
			}

			this.AddSpaceDown(105);
			if(GUI.Button(this.AlignToRight(this.AddElement(0,0,100,100), 5), "Λ"))
			   onClick(typeof(Wiki));
		}

		void PlayerProperties(){
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Name: " + this.unitSelected.nameUnit, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Price: " + this.unitSelected.player.price, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Range: " + this.unitSelected.player.range, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Damage: " + this.unitSelected.player.damage, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Cadence: " + this.unitSelected.player.cadence, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Bullet Speed: " + this.unitSelected.player.bulletSpeed, this.labelStyle);
		}
		void EnemyProperties(){
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Name: " + this.unitSelected.nameUnit, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Health: " + this.unitSelected.enemy.health, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Velocity: " + this.unitSelected.enemy.velocity, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Money dropped: " + this.unitSelected.enemy.money, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Score dropped: " + this.unitSelected.enemy.score, this.labelStyle);
			GUI.Label(this.AddElement(this.margin + 5,0,100,20), "Heal?: " + this.unitSelected.enemy.heal, this.labelStyle);
		}
	}
}

