﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace DefendThePath{
	public class MenuController : MonoBehaviour {
		public GUISkin skinHUD;

		public List<Scene> scenes = new List<Scene>();
		public int initialScene = 0;
		private int currentScene = 0;

		void Awake(){
			this.currentScene = this.initialScene;
		}

		void OnGUI(){
			GUI.skin = skinHUD;
			GUI.Label(new Rect(0,0,Screen.width, 100), "Defend The Path");
			this.scenes[this.currentScene].Render(OnClick);
			GUI.skin = skinHUD;
		}

		//Search in the scenes list for a type equals to passed. When they find the type then put the index in curretSelection
		void OnClick(Type sceneType){
			int i;
			for(i = 0; i < this.scenes.Count; i++){
				if(sceneType == this.scenes[i].GetType()){
					this.scenes[this.currentScene].OnExit();
					currentScene = i;
					this.scenes[this.currentScene].OnEnter();
					break;
				}
			}
		}
	}
}