using UnityEngine;
using System.Collections;
using System;

namespace DefendThePath{
	public class HUD : MonoBehaviour{
		[Range(0,1000)]
		public int twoStar = 0;
		[Range(0,1000)]
		public int oneStar = 0;

		private static Grid grid;
		private static Statistics statistics;
		private static GameObject selected = null;
		private static SelectionMenu selectionMenu;

		//Selections event
		public delegate void Select(GameObject unit);
		public event Select OnSelect;
		public delegate void Deselect();
		public event Deselect OnDeselect;

		public static bool gameOver = false;

		// Uese this for initialization
		void Awake (){
			try{
				grid = GameObject.Find("_Logic").GetComponent<Grid>();
				statistics = this.GetComponent<Statistics>();
				selectionMenu = this.GetComponent<SelectionMenu>();
			}catch(NullReferenceException e){
				Debug.LogError(e.Message + e.StackTrace);
				Debug.Break();
			}
		}


		void FixedUpdate(){
			if(followTexture){
				FollowMouse();
			}
		}
		Square unitDraggedSquare;
		void Update(){
			if(HUD.gameOver)
				return;

			//When the player put the unit he doesn't select the unit. So if he put he only do this.
			if(followTexture && Input.GetMouseButtonDown(0) && !Grid.editorModeStatic){
				unitDraggedSquare = grid.SearchSquare(Input.mousePosition);
				if((unitDraggedSquare.GetIsPath() && HUD.GetSelection().GetComponent<PlayerUnit>().isMine) || !unitDraggedSquare.GetIsTaken() ){
					//In editor mode the units are instaced by Grid class
					SetDraggedUnit(unitDraggedSquare);
				}
			}else{ 
				if(Input.GetMouseButtonDown(0) && !Grid.editorModeStatic)
					OnMouseDownLeft();
				if(Input.GetMouseButtonDown(1))
					OnMouseDownRight();
			}

			this.Shortcuts();
		}

		#region Drag&DropUnit
		static GameObject unitDragged;
		static GameObject unitDraggedTexture;
		static bool followTexture = false;
		public static void DragItem(GameObject unit){
			//Drop the last item
			DropItem();

			//Create a new GameObject with only the texture of the unit
			unitDraggedTexture = new GameObject("DraggedItem");

			//Set the texture and the "true" unit
			if(unit.GetComponent<PlayerUnit>() != null)
				unitDraggedTexture.AddComponent<SpriteRenderer>().sprite = unit.GetComponent<PlayerUnit>().GetSprite();
			else
				unitDraggedTexture.AddComponent<SpriteRenderer>().sprite = unit.GetComponent<SpriteRenderer>().sprite;

			unitDragged = unit;

			//Spawn the circle and scale 
			if(unit.GetComponent<PlayerUnit>() && unit.GetComponent<PlayerUnit>().prefabCircleRange != null){
				GameObject circle = Instantiate(unit.GetComponent<PlayerUnit>().prefabCircleRange, unitDraggedTexture.transform.position, unitDraggedTexture.transform.rotation) as GameObject;
				circle.transform.parent = unitDraggedTexture.transform;
				circle.transform.localScale = new Vector3(unit.GetComponent<PlayerUnit>().range, unit.GetComponent<PlayerUnit>().range, 1);
			}

			//Set initial position for spawning in the correct position.
			FollowMouse();
			followTexture = true;
		}

		public static void DropItem(){
			followTexture = false;

			Destroy(unitDraggedTexture);
			unitDraggedTexture = null;
			unitDragged = null;
		}
		public static GameObject SetDraggedUnit(Square square){
			//Substract the price to player money
			HUD.SubtractMoney(unitDragged.GetComponent<PlayerUnit>().price);
			Core.IncrementVariable(PlayerPrefsName.MoneySpent, unitDragged.GetComponent<PlayerUnit>().price);
			//Instantiate the unit in the square
			GameObject tempUnit = Instantiate(unitDragged, Camera.main.ScreenToWorldPoint(square.GetInitPosition3Corrected()), unitDraggedTexture.transform.rotation) as GameObject;
			//Set the square taken and the content as the unit
			square.SetIsTaken(true, tempUnit);
			//Drop the actual selection
			DropItem();

			return tempUnit;
		}

		private static void FollowMouse(){
			try{
				unitDraggedTexture.transform.position = Camera.main.ScreenToWorldPoint(grid.GetActualSquare().GetInitPosition3Corrected());
			}catch(NullReferenceException){
				Debug.Log("Mouse out screen");
			}
		}
		#endregion

		public static void HideGrid(){
			grid.HideGrid();
		}
		public static void ShowGrid(){
			grid.ShowGrid();
		}

		#region Selection
		private void OnMouseDownLeft(){
			if(!CheckMouseClick())
				SelectUnit();
		}
		private bool CheckMouseClick(){
			if(selectionMenu.IsInside(Input.mousePosition) && selectionMenu.drawMenu)
				return true;

			return false;
		}
		private void SelectUnit(){
			//Get content of the actual square
			Square square = grid.SearchSquare(Input.mousePosition);
			if(square == null)
				return;
			GameObject squareContent = square.GetContent();
			
			if(selected != null && selected.GetComponent<PlayerUnit>()){
				selected.GetComponent<PlayerUnit>().Deselect();
				OnDeselect.Invoke();
			}
			selected = squareContent;

			if(squareContent != null && selected.GetComponent<PlayerUnit>()){
				if(selected.GetComponent<PlayerUnit>().isMine)
					return;
				selected.GetComponent<PlayerUnit>().Select();
				OnSelect.Invoke(selected);
			}
		}
		private void OnMouseDownRight(){
			DeselectUnit();
		}
		private void DeselectUnit(){
			DropItem();
		}

		private void DrawSelectionMenu(){
	
		}
		#endregion

		#region Static StatisticsMethods
		public static void AddScore(float amount){
			statistics.AddScore(amount);
		}
		public static float GetScore(){
			return statistics.GetScore();
		}

		public static void AddMoney(float amount){
			statistics.AddMoney(amount);
		}
		public static void SubtractMoney(float amount){
			statistics.SubtractMoney(amount);
		}
		public static float GetMoney(){
			return statistics.GetMoney();
		}

		public static void AddLives(int amount){
			statistics.AddLives(amount);
		}
		public static void SubtractLives(int amount){
			statistics.SubtractLives(amount);

			if(HUD.GetLives() <= 0){
				HUD.MapEnd(false);
				Debug.Log("You are dead");
			}
		}
		public static int GetLives(){
			return statistics.GetLives();
		}
		public static bool GetIsSelected(){
			if(unitDragged == null)
				return false;
			else
				return true;
		}
		public static GameObject GetSelection(){
			return unitDragged;
		}
		#endregion

		/// <param name="winLose">If set true show WIN, false show LOSE.</param>
		public static void MapEnd(bool winLose){
			LoadStars();
	
			HUD.gameOver = true;
			GameObject selfGameObject = GameObject.Find("_HUD");
			Time.timeScale = 0;

			if(winLose)
				selfGameObject.GetComponent<WinMenu>().render = true;
			else
				selfGameObject.GetComponent<LoseMenu>().render = true;
		}

		static void LoadStars(){
			GameObject selfGameObject = GameObject.Find("_HUD");

			int stars = 0;
			int score = (int)selfGameObject.GetComponent<Statistics>().GetScore();
			int maxScore = GameObject.Find("_Logic").GetComponent<Spawner>().maxScore;
			
			if(score == maxScore){
				stars = 3;
			}else if(score >= (maxScore - selfGameObject.GetComponent<HUD>().twoStar)){
				stars = 2;
			}else if(score >= (maxScore - selfGameObject.GetComponent<HUD>().oneStar)){
				stars = 1;
			}else{
				stars = 0;
			}
			
			PlayerPrefs.SetInt(Application.loadedLevelName + "_" + "stars", stars);
			PlayerPrefs.SetInt(Application.loadedLevelName + "_" + "score", (int)score);
		}

		void OnDestroy(){
			grid = null;
			statistics = null;
			selected = null;
			selectionMenu = null;
			unitDragged = null;
			unitDraggedTexture = null;
			followTexture = false;
			gameOver = false;
		}

		void Shortcuts(){
			//Go to menu
			if(Input.GetKeyUp(KeyCode.Escape))
				Application.LoadLevel("menu");
			//Restart
			if(Input.GetKeyUp(KeyCode.R))
				Application.LoadLevel(Application.loadedLevelName);

		}

		void OnGUI(){
			GUI.Label(new Rect(5,0,100,20), "R - Restart");
			GUI.Label(new Rect(5,20,100,20), "Esc - Go to menu");
		}
	}
}	