using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class WinMenu : EndMapMenu {
		protected override void OnRender(){
			Core.IncrementVariable<int>(PlayerPrefsName.Wins);
		}
	}	
}

