using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class SelectionMenu : MonoBehaviour{
		[Range(0,100)]
		public float xPos = 0;
		[Range(0,100)]
		public float yPos = 0;

		[Range(50, 300)]
		public float width = 50;

		[Range(0, 20)]
		public float margin = 5;

		Element unit;
		public bool drawMenu {get; set;}

		// Use this for initialization
		void Start (){
			this.GetComponent<HUD>().OnSelect += Select;
			this.GetComponent<HUD>().OnDeselect += Deselect;


		}

		void Select(GameObject unit){
			this.yPosLastElement = 0;
			this.unit = new Element(AddElement(40), unit);
			this.drawMenu = true;
		}
		void Deselect(){
			this.unit = null;
			this.drawMenu = false;
		}

		private float yPosLastElement = 0;

		void OnGUI(){
			if(HUD.gameOver || !drawMenu)
				return;

			GUI.Box(new Rect(this.xPos,this.yPos, this.width, this.yPosLastElement + this.margin), GUIContent.none);

			this.yPosLastElement = 0;

			this.unit.Draw();
			this.IncreasePosLastElement(this.unit.GetBox().height);

			GUI.Label(AddElement(20), "Name: " + unit.GetPlayerScript().nameUnit);
			GUI.Label(AddElement(20), "Price: " + unit.GetPlayerScript().price);
			AddSpace(10);

			GUI.Label(AddElement(20), "Range level: " + unit.GetPlayerScript().levelRange);
			if(GUI.Button(AddElement(20,20, this.xPos + this.width - 20 - this.margin),"+") && Input.GetMouseButtonUp(0) && HUD.GetMoney() >= 150){
				if(this.unit.GetPlayerScript().UpgradeLevel(0)){
					HUD.SubtractMoney(150);
					Core.IncrementVariable<int>(PlayerPrefsName.MoneySpent, 150);
				}
			}
			GUI.Label(AddElement(20), "Price: 150");
			AddSpace(30);

			GUI.Label(AddElement(20), "Damage level: " + unit.GetPlayerScript().levelDamage);
			if(GUI.Button(AddElement(20,20, this.xPos + this.width - 20 - this.margin),"+") && Input.GetMouseButtonUp(0) && HUD.GetMoney() >= 200){
				if(this.unit.GetPlayerScript().UpgradeLevel(1)){
					HUD.SubtractMoney(200);
					Core.IncrementVariable<int>(PlayerPrefsName.MoneySpent, 200);
				}
			}
			GUI.Label(AddElement(20), "Price: 200");
			AddSpace(30);

			GUI.Label(AddElement(20), "Cadence level: " + unit.GetPlayerScript().levelCadence);
			if(GUI.Button(AddElement(20,20, this.xPos + this.width - 20 - this.margin),"+") && Input.GetMouseButtonUp(0) && HUD.GetMoney() >= 250){
				if(this.unit.GetPlayerScript().UpgradeLevel(2)){
					HUD.SubtractMoney(250);
					Core.IncrementVariable<int>(PlayerPrefsName.MoneySpent, 250);
				}
			}
			GUI.Label(AddElement(20), "Price: 250");
			AddSpace(30);

			GUI.Label(AddElement(20), "BulletSpeed level: " + unit.GetPlayerScript().levelBulletSpeed);
			if(GUI.Button(AddElement(20,20, this.xPos + this.width - 20 - this.margin),"+") && Input.GetMouseButtonUp(0) && HUD.GetMoney() >= 50){
				if(this.unit.GetPlayerScript().UpgradeLevel(3)){
					HUD.SubtractMoney(50);
					Core.IncrementVariable<int>(PlayerPrefsName.MoneySpent, 50);
				}
			}
			GUI.Label(AddElement(20), "Price: 50");
			AddSpace(30);

			if(GUI.Button(AddElement(30,this.width - this.margin * 2, this.xPos + this.margin),"$") && Input.GetMouseButtonUp(0)){
				this.unit.GetPlayerScript().Sell();
				this.Deselect();
			}
			AddSpace(30 + this.margin);
		}
		private float IncreasePosLastElement(float amount){
			this.yPosLastElement += amount;
			return amount;
		}

		private Rect AddElement(float height){
			return new Rect(this.xPos + this.margin, this.yPos + this.margin + this.yPosLastElement, this.width - margin*2, this.IncreasePosLastElement(height));
		}

		//Making xPos value default 0
		private Rect AddElement(float height, float width){
			return AddElement(height, width, this.xPos + this.margin);
		}
		private Rect AddElement(float height, float width, float xPos){
			return new Rect(xPos, this.yPos + this.margin + this.yPosLastElement, width, height);
		}

		private void AddSpace(float height){
			GUI.Label(AddElement(height), GUIContent.none);
		}

		public bool IsInside(Vector3 mouse){

			if(mouse.x >= this.xPos && mouse.x <= this.width + this.xPos && (Screen.height - mouse.y) >= this.yPos && (Screen.height - mouse.y) <= this.yPosLastElement + this.margin + this.yPos)
				return true;
			else
				return false;
		}
	}
}
