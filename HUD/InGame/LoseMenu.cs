using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class LoseMenu : EndMapMenu{
		protected override void OnRender(){
			Core.IncrementVariable<int>(PlayerPrefsName.Loses);
		}
	}
}

