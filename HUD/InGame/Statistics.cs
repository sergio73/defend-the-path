﻿using UnityEngine;
using System.Collections;

	namespace DefendThePath{
	public class Statistics : MonoBehaviour {
		public float startingMoney;
		public float startingScore;
		public int startingLives;

		private float money = 0;
		private float score = 0;
		private int lives = 0;

		void Awake(){
			this.money += this.startingMoney;
			this.score += this.startingScore;
			this.lives += this.startingLives;
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		void OnGUI(){
			if(Grid.editorModeStatic)
				return;
			GUI.Label(new Rect(Screen.width - 100, 10, 50, 20), "Money: ");
			GUI.Label(new Rect(Screen.width - 100, 30, 50, 20), "Score: ");
			GUI.Label(new Rect(Screen.width - 100, 50, 50, 20), "Lives: ");
			GUI.Label(new Rect(Screen.width - 50, 10, 50, 20), this.money.ToString());
			GUI.Label(new Rect(Screen.width - 50, 30, 50, 20), this.score.ToString());
			GUI.Label(new Rect(Screen.width - 50, 50, 50, 20), this.lives.ToString());
		}

		public void AddScore(float amount){
			this.score += amount;
		}
		public float GetScore(){
			return this.score;
		}

		public void AddMoney(float amount){
			this.money += amount;
		}
		public void SubtractMoney(float amount){
			this.money -= amount;
		}
		public float GetMoney(){
			return this.money;
		}

		public void AddLives(int amount){
			this.lives += amount;
		}
		public void SubtractLives(int amount){
			this.lives -= amount;
		}
		public int GetLives(){
			return this.lives;
		}
	}
}