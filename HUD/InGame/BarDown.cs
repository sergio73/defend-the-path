using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DefendThePath{
	public class BarDown : MonoBehaviour{
		private Rect box;

		[Range(0,200)]
		public int height;
		private int _hieght;

		public List<Element> units = new List<Element>();
		public List<EditorList> editorElements = new List<EditorList>();


		public static List<EditorList> editorElementsStatic = new List<EditorList>();

		[Range(0,15)]
		public int marginElements;

		private int index = 0;
		void Awake(){
			editorElementsStatic = this.editorElements;

			this._hieght = Screen.height - this.height;
			this.box = new Rect(0, this._hieght, Screen.width, height);
		}


		// Use this for initialization
		float widthDivisions = 0;
		public GUIStyle labelElementStyle;

		string[] listNames;
		void Start (){
			if(Grid.editorModeStatic){
				foreach(EditorList editorList in editorElements)
					Initialize(editorList.elements);
				HUD.ShowGrid();
			}else{
				Initialize(units);
			}

			List<string> tempList = new List<string>();
			foreach(EditorList editorList in editorElements){
				tempList.Add(editorList.listName);
			}
			this.listNames = tempList.ToArray();
		}
		private void Initialize(List<Element> elements){
			//Set the units in the bar
			//Prepare the screen
			/*
			 * Screen.width - marginElements: This reduce the "screen" for the last margin right. 
			 * (Screen.width - marginElements)/ units.Count: Divide the reduced screen in the units that is in units.
			 * (Screen.width - marginElements)/ units.Count) - marginElements: Rest the left margin.
			 */
			widthDivisions = ((Screen.width - marginElements)/ elements.Count) - marginElements;
			//Initialize all units
			float left = 0;
			foreach(Element element in elements){
				left += marginElements;
				element.Initialize(new Rect(left, (Screen.height - box.height) + marginElements, widthDivisions, box.height - marginElements * 2));
				left += widthDivisions;
			}
			//Set the label style for all units
			Element.labelStyle = labelElementStyle;
		}

		private bool showMenu = false;
		void OnGUI(){
			if(showMenu && !HUD.GetIsSelected()){
				//Bottom bar
				DisplayBox();
				if(Grid.editorModeStatic)
					DisplayLists();

				if(Grid.editorModeStatic)
					ShowElements(editorElements[index].elements);
				else
					ShowElements(units);
			}else{
				//Show button to display menu
				HidingBox();
			}
		}
		
		void DisplayBox(){
			//Background
			GUI.depth = 0;
			GUI.Box(new Rect(box), GUIContent.none);

			if(GUI.Button(new Rect(box.width - 70, Screen.height - box.height - 20, 50, 20), "V") && !HUD.gameOver && (Input.GetMouseButtonUp(0) || (Input.GetMouseButtonUp(2) && Grid.editorModeStatic))){
				if(!Grid.editorModeStatic)
					HUD.HideGrid();
				showMenu = false;
			}
		}
		void HidingBox(){
			//Show button
			GUI.depth = 0;
			if(GUI.Button(new Rect(box.width - 70, Screen.height - 20, 50, 20), "Λ") && !HUD.gameOver && (Input.GetMouseButtonUp(0) || (Input.GetMouseButtonUp(2) && Grid.editorModeStatic))){
				HUD.ShowGrid();
				showMenu = true;
			}
		}

		void ShowElements(List<Element> elements){
			foreach(Element element in elements){
				element.DrawButton();
			}
		}

		void DisplayLists(){
			GUI.Box(new Rect(Screen.width - 200, 25, 195, 300), GUIContent.none);
			this.index = GUI.SelectionGrid(new Rect(Screen.width - 195, 30, 185, 290), this.index, this.listNames,1);
		}

	}
}
