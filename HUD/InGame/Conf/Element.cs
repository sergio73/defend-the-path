using UnityEngine;
using System.Collections;

namespace DefendThePath{
	[System.Serializable]
	public class Element{
		[HideInInspector]
		public Rect box;
		public GameObject unit;

		public static GUIStyle labelStyle;

		private Texture2D texture;
		private PlayerUnit playerUnit;

		private PlayerUnit unitScript;

		public Element(Rect box, GameObject unit){
			this.box = box;
			this.unit = unit;

			this.playerUnit = unit.GetComponent<PlayerUnit>();
			this.texture = this.playerUnit.GetTextureHighRes();
		}
		//When it's added by inspector it call to default constructor but as in this class there is not I made this method.
		public void Initialize(Rect box){
			this.box = box;
			this.playerUnit = unit.GetComponent<PlayerUnit>();

			if(this.playerUnit != null)
				this.texture = this.playerUnit.GetTextureHighRes();
			else
				this.texture = Core.GetTextureDetail(this.unit);
		}
		public void DrawButton(){
			if(this.playerUnit){
				GUI.Label(new Rect(box.x, box.y + 10, box.width, 0), playerUnit.nameUnit, labelStyle);
				GUI.Label(new Rect(box.x, box.y + box.height - 10, box.width, 0), playerUnit.price.ToString(), labelStyle);
			}

			if((GUI.Button(box, texture) && (Input.GetMouseButtonUp(0) || (Input.GetMouseButtonUp(2) && Grid.editorModeStatic)) && !HUD.gameOver)){
				//If exist the script and have enough money set the unit. If not exist how is not an player unit it's going be impossible acces in normal menu so isn't important
				if(this.playerUnit){
					if(this.playerUnit.price <= HUD.GetMoney())
						HUD.DragItem(this.unit);
				}else{
					HUD.DragItem(this.unit);
				}
			}
		}
		public void Draw(){
			GUI.Box(box, texture);
		}
		public Rect GetBox(){
			return this.box;
		}
		public GameObject GetUnit(){
			return this.unit;
		}


		public PlayerUnit GetPlayerScript(){
			return this.playerUnit;
		}
	}
}
