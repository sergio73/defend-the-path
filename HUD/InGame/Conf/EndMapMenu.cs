using System;
using UnityEngine;
using System.Collections.Generic;

namespace DefendThePath{
	[System.Serializable]
	public class EndMapMenu : MonoBehaviour{
		public string message;
		[Range(16,50)]
		public int sizeFontMessage;

		public bool buttonMenu;
		public bool buttonRestart;
		public bool buttonNext;
		[Range(0,100)]
		public int buttonsSize;

		[Range(0,960)]
		public int width;
		[Range(0,600)]
		public int height;

		[Range(0,600)]
		public int margins;

		public Texture2D starFull;
		public Texture2D starVoid;


		public GUISkin skin;
		private GUIStyle title;
		private GUIStyle labelStyle;

		private int marginX;
		private int marginY;

		private Map map; 


		private int elements;
		private int marginElements;
		private int marginElementsActual;

		void Start(){
			this.map = new Map(this.starFull, this.starVoid);

			this.title = new GUIStyle(this.skin.GetStyle("LabelStyle"));
			this.title.alignment = TextAnchor.MiddleCenter;
			this.title.fontSize = this.sizeFontMessage;

			this.labelStyle = new GUIStyle(this.skin.GetStyle("LabelStyle"));
		}
		float marginYActual = 0;
		bool firstRender = true;

		protected virtual void OnRender(){}
		void OnGUI(){
			if(!this.render)
				return;
			else if(this.firstRender){
				this.firstRender = false;
				this.map.LoadPropertiesThisMap();
				this.OnRender();
			}

			this.marginX = (Screen.width - this.width) / 2;
			this.marginY = (Screen.height - this.height) / 2;

			this.elements = 0;
			if(this.buttonMenu)
				this.elements++;
			if(this.buttonNext)
				this.elements++;
			if(this.buttonRestart)
				this.elements++;
			this.marginElements = (this.width - (this.buttonsSize * this.elements))/(this.elements + 1);
			this.marginYActual = this.marginY;

			GUI.Box(new Rect(this.marginX, this.marginY, this.width, this.height), GUIContent.none);

			Vector2 sizeMessage = this.title.CalcSize(new GUIContent(this.message));
			GUI.Box(new Rect(this.marginX, this.marginY, this.width, sizeMessage.y), this.message, this.title);
			this.marginYActual += sizeMessage.y + this.margins;

			Vector2 scoreLabel = this.labelStyle.CalcSize(new GUIContent("Score: " + this.map.score));
			GUI.Label(new Rect((Screen.width - scoreLabel.x) / 2, this.marginYActual, scoreLabel.x, scoreLabel.y), "Score: " + this.map.score, this.labelStyle);
			this.marginYActual += scoreLabel.y + 10;

			map.DrawStars(new Rect((Screen.width - 70) / 2, this.marginYActual, 20, 20), 5);
			this.marginYActual += this.margins;

			this.marginElementsActual = this.marginElements + this.marginX;
			if(this.buttonMenu){
				if(GUI.Button(new Rect(this.marginElementsActual, this.marginYActual, this.buttonsSize, this.buttonsSize), "Menu")){
					Time.timeScale = 1;
					Application.LoadLevel("menu");
				}
				this.marginElementsActual += this.buttonsSize + this.marginElements;
			}
			if(this.buttonRestart){
				if(GUI.Button(new Rect(this.marginElementsActual, this.marginYActual, this.buttonsSize, this.buttonsSize), "Restart")){
					Time.timeScale = 1;
					Application.LoadLevel(Application.loadedLevelName);
				}
				this.marginElementsActual += this.buttonsSize + this.marginElements;
			}
			if(this.buttonNext){
				if(GUI.Button(new Rect(this.marginElementsActual, this.marginYActual, this.buttonsSize, this.buttonsSize), "Next")){
					string name = Application.loadedLevelName;//map_00

					int numberLevel = int.Parse(name.Substring(name.Length - 2)) + 1;//Take map_"00"
					name = name.Substring(0, name.Length - 2);//Take "map_"00

					string numberLeveln;
					if(numberLevel < 10)
						numberLeveln = "0" + numberLevel;
					else
						numberLeveln = numberLevel.ToString();
					Debug.Log(name + numberLeveln);

					Time.timeScale = 1;
					Application.LoadLevel(name + numberLeveln);
				}
			}
		}

		public bool render = false;
	}
}