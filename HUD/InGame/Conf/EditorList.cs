using UnityEngine;
using System.Collections.Generic;

namespace DefendThePath{
	[System.Serializable]
	public class EditorList{
		public string listName;
		public List<Element> elements = new List<Element>();
	}
}