using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class HealthBar : MonoBehaviour{
		[HideInInspector]
		public int health = 100;
		int completeHealth;

		[Range(-50,50)]
		public float xPos = 0;
		[Range(-50,50)]
		public float yPos = 0;

		[Range(-50,50)]
		public float width = 20;
		[Range(-50,50)]
		public float height = 5;

		public Color colorHealth;
		public Color colorDamage;

		private Texture2D textureHealth;
		private Texture2D textureDamage;

		private GUIStyle styleHealth = new GUIStyle();
		private GUIStyle styleDamage = new GUIStyle();

		void Start(){
			completeHealth = health;

			//Texture Health
			textureHealth = new Texture2D(1,1);
			textureHealth.SetPixel(0,0,colorHealth);
			textureHealth.Apply();

			styleHealth.normal.background = textureHealth;


			//Texture Damage
			textureDamage = new Texture2D(1,1);
			textureDamage.SetPixel(0,0,colorDamage);
			textureDamage.Apply();
			
			styleDamage.normal.background = textureDamage;
		}


		void OnGUI(){
			Vector3 positionScreen = Camera.main.WorldToScreenPoint(this.transform.position);
			Rect box = new Rect(positionScreen.x + xPos,(Screen.height - positionScreen.y) + yPos, this.width, this.height);

			//Damage background
			GUI.depth = 1;
			GUI.Box(box, GUIContent.none, styleDamage);

			//Health
			GUI.depth = 2;
			box.width = health * width / completeHealth;
			GUI.Box(box, GUIContent.none, styleHealth);
		}
	}
}