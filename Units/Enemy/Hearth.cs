using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class Hearth : MonoBehaviour{
		public int healing = 0;

		//Check when hearth collision with a enemy
		void OnTriggerEnter2D(Collider2D collider){
			if(collider.tag != "EnemyHitBox")
				return;

			//Double parent because the object is Enemy->Container->HitBox and I need acces to enemy
			EnemyUnit enemy = collider.transform.parent.parent.GetComponent<EnemyUnit>();

			//The heart doesn't heal to enemies like ambulance
			if(enemy.heal)
				return;
			//Heal the unit
			if(!enemy.Heal(this.healing))
				return;

			//If heal the unit then destroy the hearth
			Destroy(this.gameObject);
		} 
	}
}