using UnityEngine;
using System;
using System.Collections.Generic;

namespace DefendThePath{
	public class EnemyUnit : MonoBehaviour{
		public string nameEnemy;
		[Range(0,1000)]
		public int health;
		[Range(0,2)]
		public float velocity;
		[Range(0,1000)]
		public int money;
		[Range(0,1000)]
		public int score;
		[Range(0,1)]
		public float timeBetweenNodes = 0.1F;
		private float lastTimeBetweenNodes;

		public bool heal;
		public GameObject hearthPrefab;
		[Range(1,60)]
		public float ratioDropHearth;
		[Range(1,10)]
		public float ratioDropHearthRandom;

		[Range(1,200)]
		public int healing;

		public Texture2D highResTexture;

		private Vector3 nextNodePosition {get; set; }
		private int nextNodeNumber = 0;
		private Vector3 dir;
		private HealthBar healthBar;
		private int completeHealth;

		public delegate void OnDead();
		public event OnDead OnDeadEvent;

		private Spawner spawner;

		void Start(){
			//Start with initial random time
			this.timeSpawnHearth = UnityEngine.Random.Range(-this.ratioDropHearthRandom,this.ratioDropHearthRandom);

			spawner = GameObject.Find("_Logic").GetComponent<Spawner>();

			//Equals health
			this.completeHealth = this.health;

			//Prepare the healthbar
			healthBar = this.GetComponent<HealthBar>();
			healthBar.health = this.health;

			//Add the first node
			AddNextNode();

			//Set events inside class
			OnDeadEvent += OnDeadMethod;
		}

		void Update(){
			if(Debug.isDebugBuild)
				Debug.DrawLine(this.transform.position, nextNodePosition);
			SpawnHearth();
		}

		void FixedUpdate(){
			this.transform.position += dir * velocity * Time.fixedDeltaTime;
		}


		//This way check taht minimum has been passed 0.1 seconds
		public void NextNode() {
			if((Time.time - lastTimeBetweenNodes) < timeBetweenNodes)
				return;
			lastTimeBetweenNodes = Time.time;
			AddNextNode();
		}
		//This method add the next node and calculate the new direction.
		private void AddNextNode(){
			if(spawner.GetNodesQty() == nextNodeNumber){
				HUD.SubtractLives(1);
				Spawner.EnemyDead();
				Destroy(this.gameObject);
				return;
			}
			nextNodeNumber += 1;
			nextNodePosition = spawner.GetNextNode(nextNodeNumber);
			CalcRotation();
		}

		private void CalcRotation(){
			dir = nextNodePosition - this.transform.position;
			dir.z = 0;
			dir.Normalize();

			//Atan2 takes the information given with the two arguments. The purpose of using two arguments instead of one is gather information on the signs of the inputs in order to return the appropiate
			//quadrant of the computed angle, wich is not possible for the single-argument Atan function
			//http://en.wikipedia.org/wiki/Atan2

			this.transform.rotation = Quaternion.Euler(new Vector3(0,0,Core.Vector3DToAngles(this.dir)));
		}

		private float timeSpawnHearth;
		private void SpawnHearth(){
			if(this.heal && (Time.time - this.timeSpawnHearth) > this.ratioDropHearth){
				this.timeSpawnHearth = Time.time + UnityEngine.Random.Range(-this.ratioDropHearthRandom,this.ratioDropHearthRandom);
			}else{
				return;
			}

			GameObject hearth = Instantiate(this.hearthPrefab, this.transform.position, new Quaternion(0,0,0,0)) as GameObject;
			hearth.GetComponent<Hearth>().healing = this.healing;
		}

		/// <summary>
		/// Heal the specified health. If the health is complete then return false if not return true.
		/// </summary>
		/// <param name="health">Health.</param>
		public bool Heal(int health){
			if(this.health >= this.completeHealth)
				return false;

			int result;
			if((result = (this.completeHealth - this.health)) < health){
				this.health += result;
				this.healthBar.health += result;
			}else{
				this.health += health;
				this.healthBar.health += health;
			}
			return true;
		}

		//Set a new health, if the health is less than zero then the enemy die
		public void AddDamage(int damage){
			//Prevent double count when two bullets impact at same time
			if(this.health <= 0)
				return;

			int tempHealth = this.health - damage;
			if(tempHealth <= 0){
				//How the destroy is not instant the second bullet will destroy the object, so we have to set the health to 0.
				this.health = 0;
				OnDeadEvent();
			}else{
				this.health = tempHealth;
				this.healthBar.health = tempHealth;
			}
		}

		//Detect the nodes and recalculates the direction
		void OnTriggerEnter2D(Collider2D collider){
			if(collider.tag != "Node")
				return;
			this.NextNode();
		}

		void OnDeadMethod(){
			Core.IncrementVariable<int>(PlayerPrefsName.EnemyDead);
			Core.IncrementVariable<int>(PlayerPrefsName.Money, this.money);
			Core.IncrementVariable<int>(PlayerPrefsName.Score, this.score);
			HUD.AddScore(this.score);
			HUD.AddMoney(this.money);
			Spawner.EnemyDead();

			if(this.heal){
				GameObject hearth = Instantiate(this.hearthPrefab, this.transform.position, new Quaternion(0,0,0,0)) as GameObject;
				hearth.GetComponent<Hearth>().healing = this.healing * 3;
			}
			Destroy(this.gameObject);
		}
		public Sprite GetSprite(){
			return this.gameObject.transform.FindChild("ContainerBase").FindChild("Texture").GetComponent<SpriteRenderer>().sprite;
		}
		public Texture2D GetTexture(){
			return this.gameObject.transform.FindChild("ContainerBase").FindChild("Texture").GetComponent<SpriteRenderer>().sprite.texture;
		}
		public Texture2D GetTextureHighRes(){
			return this.highResTexture;
		}
	}
}

