/*
 * Define the basic player unit
 */
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

namespace DefendThePath{
	[RequireComponent(typeof(AudioSource))]
	[RequireComponent(typeof(UpdateSound))]
	public class PlayerUnit : MonoBehaviour{
		//This is the relation between the circleRange and the collider. The result is a rule of three 0.5 - 1.27 | 0.2 - X #0.2 * 1.27 = 0.254 #0.254 
		private const float relationCircleRangeCollider = 2.54F;

		//General properties
		public string nameUnit;
		[Range(0,2000)]
		public int price;
		[Range(0,2)]
		public float range;
		[Range(0,2)]
		public float improvedByLevelRange;
		[Range(0,500)]
		public int damage;
		[Range(0,500)]
		public int improvedByLevelDamage;
		[Range(0,5)]
		public float cadence;
		[Range(0,2)]
		public float improvedByLevelCadence;
		[Range(0,5)]
		public float bulletSpeed;
		[Range(0,5)]
		public float improvedByLevelBulletSpeed;

		public bool isMine = false;

		public GameObject bulletPrefab;
		//Position for spawn bullets
		public List<Transform> bulletSpawn = new List<Transform>();

		//Prefab for range circle
		public GameObject prefabCircleRange;


		public Texture2D highResTexture;

		//Instantite for the prefabCircleRange when the player clicks
		private GameObject circleRange;
		//It's the child that contains graphics, and where bullets spawns
		private GameObject container;
		//When the enemy enter to range OnTriggerEnter saves a reference
		private GameObject target;
		private AudioSource shotSound;

		//Levels
		public byte levelRange {get; set;}
		public byte levelDamage {get; set;}
		public byte levelCadence {get; set;}
		public byte levelBulletSpeed {get; set;}


		void Awake(){
			this.container = this.transform.FindChild("ContainerBase").gameObject;
			this.RedimensioneCollider();
		}

		void Start(){
			if(!this.isMine){
				this.shotSound = this.GetComponent<AudioSource>();
				this.shotSound.playOnAwake = false;
			}
		}

		void Update(){
			Shoot();
		}

		void FixedUpdate(){
			Rotate();
		}

		//Check if target is not null and rotate in target direction.
		void Rotate(){
			if(target != null){
				this.container.transform.rotation = Quaternion.Euler(0,0,Core.CalculeAngle(this.transform.position, this.target.transform.position));
			}
		}

		//Actions
		public void Select(){
			if(this.isMine)
				return;

			if(this.prefabCircleRange == null)
				return;
			if(this.circleRange == null)
				this.circleRange = Instantiate(this.prefabCircleRange, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + 0.1F), this.transform.rotation) as GameObject;
			this.circleRange.transform.parent = this.transform;
			this.circleRange.name = "Circle";
			this.circleRange.transform.localScale = new Vector3(range, range, 1);
		}
		public void RedimensioneCollider(){
			if(this.collider2D)
				this.GetComponent<CircleCollider2D>().radius = range * relationCircleRangeCollider;
		}
		public void Deselect(){
			Destroy(this.circleRange);
		}

		private float timeCadence = 0;
		private bool firstEnter = true;
		private void Shoot(){
			if(this.target != null && (Time.time - this.timeCadence) > this.cadence){
				//Add little delay for the first enemy eneter, this way the bullet does not spawn in the last direction.
				if(this.firstEnter){
					this.firstEnter = false;
					this.timeCadence = Time.time - 0.01F;
					return;
				}
				

				float delay = 0;
				foreach(Transform spawn in bulletSpawn){
					StartCoroutine(InstantiteBulletDelay(delay, spawn, this.bulletSpeed, this.damage));
					delay += this.cadence;
				}
				//Wait the same time between the new loop
				this.timeCadence = Time.time + delay - this.cadence;
			}
		}

		/// <summary>
		/// Instantites the bullet delay.
		/// </summary>
		/// <returns>The bullet delay.</returns>
		/// <param name="delay">Delay.</param>
		/// <param name="spawn">Spawn.</param>
		/// <param name="bulletSpeed">Bullet</param>
		/// <param name="damage">Damage.</param>
		private IEnumerator InstantiteBulletDelay(float delay, Transform spawn, float bulletSpeed, int damage){
			yield return new WaitForSeconds(delay);
			if(this.target == null)
				yield break;
			Bullet bullet = (Instantiate(bulletPrefab, spawn.position, this.container.transform.rotation) as GameObject).GetComponent<Bullet>();
			bullet.speed = bulletSpeed;
			bullet.damage = damage;
			this.shotSound.Play();
			Core.IncrementVariable<int>(PlayerPrefsName.Shots);
		}

		public void Sell(){
			HUD.AddMoney(Mathf.Round(this.price/2));
			GameObject.Find("_Logic").GetComponent<Grid>().SearchSquare(Camera.main.WorldToScreenPoint(this.transform.position)).SetIsTaken(false);
			Destroy(this.gameObject);
		}
		//Detect when enemy enter
		void OnTriggerEnter2D(Collider2D collider){
			if(collider.tag != "EnemyHitBox")
				return;
			if(this.isMine){
				this.MineExplode(collider);
				return;
			}
			//Desuscribe to dead event on enter
			if(this.target != null)
				this.target.transform.parent.transform.parent.GetComponent<EnemyUnit>().OnDeadEvent -= OnEnemyDead;

			//Get the enemy
			this.target = collider.gameObject;

			//Suscribe event to the new enemy
			this.target.transform.parent.transform.parent.GetComponent<EnemyUnit>().OnDeadEvent += OnEnemyDead;
		}
		void OnTriggerExit2D(Collider2D collider){
			//Check if is an enemy or is mine
			if(collider.tag != "EnemyHitBox" || this.isMine)
				return;

			//Check that is not null because if is null after when check the hash it goes do an error. This only is useful to the first enemy,
			if(target == null)
				return;

			//Check that enemy is going out is different to the target. This is useful because when the hash is equals it waits 0.01 seconds before shoot, this way the turret can aim to the enemy.
			if(this.target.GetHashCode() != collider.gameObject.GetHashCode())
				return;

			//Desuscribe to dead event on exit
			this.target.transform.parent.transform.parent.GetComponent<EnemyUnit>().OnDeadEvent -= OnEnemyDead;

			//If is the same enemy that is in the target then prepare the unit to new enemies
			this.firstEnter = true;
			this.target = null;
		}

		void MineExplode(Collider2D collider){
			Instantiate(bulletPrefab, this.container.transform.position, this.container.transform.rotation);
			collider.transform.parent.transform.parent.GetComponent<EnemyUnit>().AddDamage(this.damage);
			Destroy(this.gameObject);
		}
		void OnEnemyDead(){
			this.firstEnter = true;
			this.target = null;

		}

		/// <summary>
		/// <para>Upgrades the level of a unit.</para>
		/// <param name="propertie">
		/// <para>0 = levelRange;</para>
		/// <para>1 = levelDamage;</para>
		/// <para>2 = levelCadence;</para>
		/// <para>3 = levelBulletSpeed;</para>
		/// </param>
		/// </summary>
		public bool UpgradeLevel(byte propertie){
			switch(propertie){
			case 0:
				if(this.levelRange >= 3)
					return false;

				this.levelRange += 1;
				this.range += this.improvedByLevelRange;
				this.RedimensioneCollider();
				this.Select();
				return true;

			case 1:
				if(this.levelDamage >= 3)
					return false;

				this.levelDamage += 1;
				this.damage += this.improvedByLevelDamage;
				return true;

			case 2:
				if(this.levelCadence >= 3)
					return false;

				this.levelCadence += 1;
				this.cadence -= this.improvedByLevelCadence;
				return true;

			case 3:
				if(this.levelBulletSpeed >= 3)
					return false;

				this.levelBulletSpeed += 1;
				this.bulletSpeed += this.improvedByLevelBulletSpeed;
				return true;
			}
			return false;
		}

		public Sprite GetSprite(){
			return this.gameObject.transform.FindChild("ContainerBase").FindChild("Texture").GetComponent<SpriteRenderer>().sprite;
		}
		public Texture2D GetTexture(){
			return this.gameObject.transform.FindChild("ContainerBase").FindChild("Texture").GetComponent<SpriteRenderer>().sprite.texture;
		}
		public Texture2D GetTextureHighRes(){
			return this.highResTexture;
		}
	}
}

