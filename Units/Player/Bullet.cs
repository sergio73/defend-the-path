using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class Bullet : MonoBehaviour{
		public float speed;
		public int damage;
		private float timeToLive = 10;
		private float time_timeToLive;

		// Use this for initialization
		void Start (){
			time_timeToLive = Time.time;
			if(particleSystem){
				particleSystem.renderer.sortingOrder = 10;
			}
		}

		// Update is called once per frame
		void Update (){
			if((Time.time - time_timeToLive) > timeToLive)
				Destroy(this.gameObject);
		}
		void FixedUpdate(){
			this.transform.position += this.transform.right * speed * Time.fixedDeltaTime;
		}

		//Check when bullet collision with a enemy
		void OnTriggerEnter2D(Collider2D collider){
			if(collider.tag != "EnemyHitBox")
				return;
			//Double parent because the object is Enemy->Container->HitBox and I need acces to enemy
			collider.transform.parent.parent.GetComponent<EnemyUnit>().AddDamage(damage);
			Destroy(this.gameObject);
		} 
	}
}

