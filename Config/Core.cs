using System;
using UnityEngine;

namespace DefendThePath{
	public class Core{
		//Takes X and Y form a 3D vector and calculate the angle in degrees
		public static int Vector3DToAngles (Vector3 dir){
			//Atan2 takes the information given with the two arguments. The purpose of using two arguments instead of one is gather information on the signs of the inputs in order to return the appropiate
			//quadrant of the computed angle, wich is not possible for the single-argument Atan function
			//http://en.wikipedia.org/wiki/Atan2

			return Convert.ToInt32(Math.Atan2(dir.y,dir.x)*(180/Math.PI));
		}

		//Return in degrees the angle from position to target
		public static int CalculeAngle(Vector3 from, Vector3 target){
			Vector3 dir = target - from;
			dir.z = 0;
			dir.Normalize();

			return Vector3DToAngles(dir);
		}

		public static void SaveLevelStats(){
			PlayerPrefs.SetFloat(Application.loadedLevelName + "_score", HUD.GetScore());
			PlayerPrefs.SetFloat(Application.loadedLevelName + "_money", HUD.GetMoney());
			PlayerPrefs.SetInt(Application.loadedLevelName + "_lives", HUD.GetLives());
		}

		public static Texture2D GetTextureDetail(GameObject element){
			return element.gameObject.GetComponent<SpriteRenderer>().sprite.texture;
		}


		/// <summary>
		/// Increments a global variable.
		/// </summary>
		/// <param name="name">Name.</param>
		public static void IncrementVariable<T>(string name){
			if(typeof(T) == typeof(int))
				PlayerPrefs.SetInt(name, PlayerPrefs.GetInt(name, 0) + 1);
			else if(typeof(T) == typeof(float))
				PlayerPrefs.SetFloat(name, PlayerPrefs.GetFloat(name, 0) + 1);
			else
				Debug.LogError("Only can be int or float");
		}

		/// <summary>
		/// Increments a global variable.
		/// </summary>
		/// <param name="name">Name.</param>
		public static void IncrementVariable<T>(string name, T amount){
			if(typeof(T) == typeof(int))
				PlayerPrefs.SetInt(name, PlayerPrefs.GetInt(name, 0) + Convert.ToInt32(amount));
			else if(typeof(T) == typeof(float))
				PlayerPrefs.SetFloat(name, PlayerPrefs.GetFloat(name, 0) + Convert.ToInt32(amount));
			else
				Debug.LogError("Only can be int or float");
		}
	}
}

