﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DefendThePath;
using System.Threading;
using System.ComponentModel;
using System.IO;


namespace DefendThePath{
	public class Grid : MonoBehaviour {
		public GameObject gridGameObject;
		public GameObject mouseOverObject;
		public GameObject pathOverObject;

		public float gridSize = 1;
		public bool editorMode = false;
		public static bool editorModeStatic = false;

		private List<Row> rows = new List<Row>();

		//Save and read squares busies
		private StreamWriter fileGridsWriter;
		private StreamReader fileGridReader;
		private Stream file;
		//Worker(Thread) that is constantly checking that this square is the mouse
		BackgroundWorker checkSquaresWorker = new BackgroundWorker();

		public bool showGrid = false;
		public void ShowGrid(){
			gridGameObject.transform.position = new Vector3(0,0,0);
			showGrid = true;
		}
		public void HideGrid(){
			gridGameObject.transform.position = new Vector3(10,0,0);
			//Hide texture mouseOver
			mouseOverObject.transform.position = new Vector3(10,0,0);

			showGrid = false;
		}

		void Awake(){
			//Read the file with all taken squares.
			try{
				file = new MemoryStream(((TextAsset)Resources.Load(Application.loadedLevelName, typeof(TextAsset))).bytes);
			}catch(System.NullReferenceException){
				Debug.LogError("This map has not found the file " + Application.loadedLevelName + ".txt in resources folder, please create the file and try again");
				Debug.Break();
			}
			//Set public mode
			editorModeStatic = this.editorMode;
		}
		//Charge squares inside of rows
		void Start () {	
			//Divide map in squares
			float axisX = 0;
			float axisY = 0;

			Row loadingRow;
			while(axisY < Screen.height){
				loadingRow = new Row(axisY, axisY + gridSize);
				while(axisX < Screen.width){
					loadingRow.AddSquare(new Square(axisX, axisX + gridSize, axisY, axisY + gridSize));
					axisX += gridSize;
				}
				rows.Add(loadingRow);
				axisX = 0;
				axisY += gridSize;
			}

			checkSquaresWorker.DoWork += new DoWorkEventHandler(CheckSquares_DoWork);
			//Read the .txt that contains the squares busies or Write if is in editor mode also if is in read mode spawn the map details.
			if(editorMode){
				ReadFile();
				fileGridReader.Close();
				fileGridsWriter = new StreamWriter("Assets\\Resources\\" + Application.loadedLevelName + ".txt");
			}else{
				ReadFile();
			}
		}

		private void ReadFile(){
			fileGridReader = new StreamReader(file);
			string line;
			while((line = fileGridReader.ReadLine()) != null){
				string[] lineSplitted = line.Split(new char[] {','});
				if(lineSplitted[1] != "void" && lineSplitted[1] != "path"){
					//Compare the name with the possibilities in the bar down menu
					foreach(EditorList elementList in BarDown.editorElementsStatic){
							foreach(Element element in elementList.elements){
							if(element.GetUnit().name == lineSplitted[1]){
								if(editorMode){
									listSquare.Add(new DataCNT(lineSplitted[0], element.GetUnit().name));
									SearchAndFill(true, int.Parse(lineSplitted[0]), element.GetUnit(), true, true);
								}else{
									SearchAndFill(true, int.Parse(lineSplitted[0]), element.GetUnit(), true, false);
								}
								break;
							}
						}
					}
				}else{
					if(editorMode){
						listSquare.Add(new DataCNT(lineSplitted[0], lineSplitted[1]));
						if(lineSplitted[1] == "path")
							SearchAndSetTaken(true, int.Parse(lineSplitted[0]), true, this.pathOverObject, true);
						else
							SearchAndSetTaken(true, int.Parse(lineSplitted[0]), true, this.mouseOverObject, false);
					}else{
						if(lineSplitted[1] == "path")
							SearchAndSetTaken(true, int.Parse(lineSplitted[0]), false, null, true);
						else
							SearchAndSetTaken(true, int.Parse(lineSplitted[0]));
					}
				}
			}
		}

		#region Search
		/*
		 * Check mouse position and determine in that square is.
		 * This work is in a thread because is too taken.
		 * How in a thread can't use Unity Function because is Thread-Safe I save coords mouse in a variable.
		 */
		private Vector3 mousePosition = Vector3.zero;
		private Square squareRow;

		void CheckSquares_DoWork(object send, DoWorkEventArgs e){
			squareRow = SearchSquare(mousePosition);
		}

		// Update is called once per frame
		void Update () {
			SearchGrid();
			CreateMap();
		}

		//Actualize the square that contains the mouse. If is hover any square taken move the mouseOverObject to that square.
		void SearchGrid(){
			if(!showGrid || checkSquaresWorker.IsBusy || editorMode){
				return;
			}
			mousePosition = Input.mousePosition;
			checkSquaresWorker.RunWorkerAsync();

			//Move the object
			if(squareRow != null){
				if(squareRow.GetIsTaken())
					mouseOverObject.transform.position = Camera.main.ScreenToWorldPoint(squareRow.GetInitPosition3());
				else
					mouseOverObject.transform.position = new Vector3(10,0,0);

			}
		}
		#endregion
		#region EditorMode
		private bool endSelection;
		private List<DataCNT> listSquare = new List<DataCNT>();
		private Square tempSquare;
		void CreateMap(){
			if(!editorMode || checkSquaresWorker.IsBusy && !HUD.gameOver)
				return;
			mousePosition = Input.mousePosition;
			checkSquaresWorker.RunWorkerAsync();

			if(squareRow == null){
				return;
			}
			tempSquare = squareRow;

			if(!HUD.GetIsSelected() || !(Input.GetKey(KeyCode.LeftAlt) && Grid.editorModeStatic))
				mouseOverObject.transform.position = Camera.main.ScreenToWorldPoint(tempSquare.GetInitPosition3());
			else
				mouseOverObject.transform.position = new Vector3(10,0,0);

			//Desactivate with alt in editor mode
			if(Input.GetKey(KeyCode.LeftAlt) && Grid.editorModeStatic)
				return;

			//Instantite an prefab mouseOver in the square that is the mouse. After that, add the square and their contains to listSquare. If is not anything selected the square is saved as "void"
			if(Input.GetMouseButton(0) && !tempSquare.GetIsTaken()){
				if(HUD.GetIsSelected()){
					Instantiate(mouseOverObject, Camera.main.ScreenToWorldPoint(tempSquare.GetInitPosition3()), new Quaternion(0,0,0,0)).name = "Square_" + tempSquare.GetId().ToString();

					//Instance de unit, add an a list and after drop the selection
					SearchAndFill(true, tempSquare.GetId(), HUD.GetSelection(), true, false);
					listSquare.Add(new DataCNT(tempSquare.GetId().ToString(), HUD.GetSelection().name));
					HUD.DropItem();
				}else{
					SearchAndSetTaken(true, tempSquare.GetId());
					if(Input.GetKey(KeyCode.LeftControl)){
						Instantiate(pathOverObject, Camera.main.ScreenToWorldPoint(tempSquare.GetInitPosition3()), new Quaternion(0,0,0,0)).name = "Square_" + tempSquare.GetId().ToString();
						listSquare.Add(new DataCNT(tempSquare.GetId().ToString(), "path"));
					}else{
						Instantiate(mouseOverObject, Camera.main.ScreenToWorldPoint(tempSquare.GetInitPosition3()), new Quaternion(0,0,0,0)).name = "Square_" + tempSquare.GetId().ToString();
						listSquare.Add(new DataCNT(tempSquare.GetId().ToString(), "void"));
					}
				}
			}
			//Remove the square contains and remove of listSquare
			if(Input.GetMouseButton(1) && tempSquare.GetIsTaken()){
				if(tempSquare.GetContent() == null){
					SearchAndDestroy(listSquare, tempSquare.GetId().ToString());
				}else{
					SearchAndDestroy(listSquare, tempSquare.GetId().ToString());
				}

				Destroy(GameObject.Find("Square_" + tempSquare.GetId().ToString()));
				Destroy(tempSquare.GetContent());
				SearchAndSetTaken(false, tempSquare.GetId());
			}
		}
		//Remove elements on list of squares
		private void SearchAndDestroy(List<DataCNT> data, string id){
			foreach(DataCNT line in data){
				if(line.GetSquareID() == id){
					data.Remove(line);
					break;
				}
			}
		}

		//Buttons
		void OnGUI (){
			if(editorMode){
				GUI.Label(new Rect(Screen.width - 200, 0, 200, 20), "For spawn path press control key");
				GUI.Label(new Rect(10 , 40, 600, 20), "Press Left Alt for allow press buttons with left click without spawn box");
				if(GUI.Button(new Rect(10,10,200,25), "Finish editing")){
					int i = 0;
					while(i<listSquare.Count){
						fileGridsWriter.WriteLine(listSquare[i].GetSquareID() + "," + listSquare[i].GetSquareContent());
						i++;
					}
					fileGridsWriter.Close();
				}
				if(GUI.Button(new Rect(220,10,200,25), "Restart")){
					fileGridsWriter.Close();
					int i = 0;
					while(i<listSquare.Count){
						SearchAndSetTaken(false, int.Parse(listSquare[i].GetSquareID()), true);		
						Destroy(GameObject.Find("Square_"+listSquare[i].GetSquareID()));
						i++;
					}
					listSquare.Clear();
					fileGridsWriter = new StreamWriter("Assets\\Resources\\" + Application.loadedLevelName + ".txt");
				}
			}
		}
		#endregion

		#region PrivateMethods
		//Search the square and set as taken
		void SearchAndSetTaken(bool taken, int id){
			SearchAndSetTaken(taken, id, false, null, false);
		}
		void SearchAndSetTaken(bool taken, int id, bool destroy){
			SearchAndSetTaken(taken, id, false, null, false, destroy);
		}
		void SearchAndSetTaken(bool taken, int id, bool mouseOverInstantite, GameObject objectToInstantiate, bool isPath){
			SearchAndSetTaken(taken, id, mouseOverInstantite, objectToInstantiate, isPath, false);
		}
		void SearchAndSetTaken(bool taken, int id, bool mouseOverInstantite, GameObject objectToInstantiate, bool isPath, bool destroy){
			Square squareTemp;
			foreach(Row row in rows){
				if((squareTemp = row.SearchRow(id)) != null){
					if(mouseOverInstantite)
						(Instantiate(objectToInstantiate, Camera.main.ScreenToWorldPoint(squareTemp.GetInitPosition3()), new Quaternion(0,0,0,0))as GameObject).name = "Square_" + squareTemp.GetId().ToString();
					squareTemp.SetIsTaken(taken);
					if(isPath)
						squareTemp.SetIsPath(true);
					if(destroy)
						Destroy(squareTemp.GetContent());
				}
			}	
		}
		//Search the square, set as taken and put the game object inside. If instantiate is true make a copy of the gameobjct and after then save.
		void SearchAndFill(bool taken, int id, GameObject gameObject, bool instantiate, bool mouseOverInstantite){
			Square squareTemp;
			foreach(Row row in rows){
				if((squareTemp = row.SearchRow(id)) != null){
					if(instantiate){
						if(mouseOverInstantite)
							(Instantiate(this.mouseOverObject, Camera.main.ScreenToWorldPoint(squareTemp.GetInitPosition3()), new Quaternion(0,0,0,0))as GameObject).name = "Square_" + squareTemp.GetId().ToString();

						GameObject tempGM = Instantiate(gameObject, Camera.main.ScreenToWorldPoint(squareTemp.GetInitPosition3Corrected()), new Quaternion(0,0,0,0)) as GameObject;
						tempGM.name = gameObject.name;

						squareTemp.SetIsTaken(taken, tempGM);
					}else
						squareTemp.SetIsTaken(taken, gameObject);
				}
			}	
		}
		#endregion
		#region PublicMethods
		//Search a square by the mouse position
		public Square SearchSquare(Vector3 mousePosition){
			Square squareRowTemp = null;
			foreach(Row row in rows){
				if((squareRowTemp = row.SearchRow(mousePosition)) != null){
					break;
				}
			}
			return squareRowTemp;
		}
		//Search and square 
		public Square SearchSquare(int id){
			Square squareRowTemp = null;
			foreach(Row row in rows){
				if((squareRowTemp = row.SearchRow(id)) != null){
					break;
				}
			}
			return squareRowTemp;
		}

		public Square GetActualSquare(){
			return this.squareRow;
		}

		#endregion

		void OnDestroy(){
			foreach(Row row in rows){
				row.Destroy();
			}
			Square.ResetID();
		}
	}
}
