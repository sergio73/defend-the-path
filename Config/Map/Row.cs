using System;
using System.Collections.Generic;
using UnityEngine;

namespace DefendThePath{
	public class Row{
		private List<Square> squares = new List<Square>();
		private float xInit;
		private float xEnd;
		static private int lastId;
		private int id;

		public Row(float xInit, float xEnd){
			id = lastId;
			lastId++;
			this.xInit = xInit;
			this.xEnd = xEnd;
		}
		public void AddSquare(Square square){
			squares.Add(square);
		}
		public bool IsInside(Vector3 mousePostion){
			if(mousePostion.y >= xInit && mousePostion.y < xEnd){
				return true;
			}
			return false;
		}
		public Square SearchRow(Vector3 mousePosition){
			if(!IsInside(mousePosition))
				return null;

			foreach(Square square in squares){
				if(square.IsInside(mousePosition))
					return square;
			}
			return null;
		}
		public Square SearchRow(int id){
			foreach(Square square in squares){
				if(square.GetId() == id)
					return square;
			}
			return null;
		}
		public void Destroy(){
			foreach(Square square in squares){
				square.Dispose();
			}
		}
		public int GetID(){
			return id;
		}
	}
}

