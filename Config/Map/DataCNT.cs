//DataCNT is an abbreviation of DataContent that saves in .cnt files. This file contains all squares occuped and that it's contains
using System;
namespace DefendThePath{
	public class DataCNT{
		private string squareID;
		private string squareContent;

		public DataCNT (string squareID, string squareContent){
			this.squareID = squareID;
			this.squareContent = squareContent;
		}

		public string GetSquareID(){
			return this.squareID;
		}
		public string GetSquareContent(){
			return this.squareContent;
		}
		public void SetSquareID(string squareID){
			this.squareID = squareID;
		}
		public void SetSquareContent(string squareContent){
			this.squareContent = squareContent;
		}
	}
}

