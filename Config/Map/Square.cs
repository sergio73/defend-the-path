using System;
using UnityEngine;

//It's a class with the properties of a square grid.
namespace DefendThePath{
	public class Square : IDisposable{
		private static int idSquare = 0;
		private int id;
		private Vector2 axisX;
		private Vector2 axisY;
		private bool isTaken = false;
		private GameObject content = null;
		private bool isPath;
		private bool disposed;

		public Square (float xInit, float xEnd, float yInit, float yEnd){
			axisX = new Vector2(xInit, xEnd);
			axisY = new Vector2(yInit, yEnd);
			id = idSquare;
			idSquare += 1;
		}

		//If the mouse is inside of the square return true
		public bool IsInside(Vector3 mousePosition){
			if(Contains(axisX, mousePosition.x) && Contains(axisY, mousePosition.y))
			   return true;
			return false;
		}
		//Return true if the content is inside of contains
		private bool Contains(Vector2 contains, float content){
			if(content >= contains.x && content <= contains.y)
				return true;
			return false;
		}

		public Vector2 GetInitPosition2(){
			return new Vector2(axisX.x, axisY.x);
		}
		public Vector3 GetInitPosition3(){
			return new Vector3(axisX.x, axisY.x, 9);
		}
		//Sum 15 to image for center in square
		public Vector2 GetInitPosition2Corrected(){
			return new Vector2(axisX.x + 15, axisY.x + 15);
		}
		public Vector3 GetInitPosition3Corrected(){
			return new Vector3(axisX.x + 15, axisY.x + 15, 9);
		}

		public int GetId(){
			return id;
		}
		public void SetIsTaken(bool isTaken){
			this.isTaken = isTaken;
		}
		public void SetIsTaken(bool isTaken, GameObject content){
			this.isTaken = isTaken;
			this.content = content;
		}

		public void SetIsPath(bool isPath){
			this.isPath = isPath;
		}
		public bool GetIsPath(){
			return this.isPath;
		}
		public bool GetIsTaken(){
			return this.isTaken;
		}
		public GameObject GetContent(){
			return this.content;
		}

		public void Dispose(){
			Dispose(true);

			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing){
			if(!this.disposed){
				if(disposing){
					//Manual release of managed resources
				}
				//Release unmanaged resources
				this.disposed = true;
			}
		}
		public static void ResetID(){
			idSquare = 0;
		}
	}
}

