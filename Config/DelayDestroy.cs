using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class DelayDestroy : MonoBehaviour {
		[Range(0,100)]
		public int timeLife = 0;
		
		void Awake() {
			Destroy(this.gameObject, this.timeLife);
		}
	}
}