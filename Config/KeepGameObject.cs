﻿using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class KeepGameObject : MonoBehaviour {
		public bool canBeDuplicatedTag = false;

		void Awake(){
			if(!this.canBeDuplicatedTag){
				GameObject[] objects = GameObject.FindGameObjectsWithTag(this.gameObject.tag);
				if(objects.Length <= 1)
					return;

				foreach(GameObject gmObject in objects){
					if(gmObject.GetHashCode() == this.gameObject.GetHashCode()){
						Destroy(this.gameObject);
					}
				}
			}
		}
		// Use this for initialization
		void Start () {
			DontDestroyOnLoad(this);
		}
	}
}