﻿using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class UpdateSound : MonoBehaviour {
		[Range(0,1)]
		public float defaultVolume = 1;

		public bool music = false;
		public bool effect = false;

		private delegate void UpdateVolume();
		UpdateVolume updateVolume;
		AudioSource sound;

		void Start(){
			if((music && effect) || (!music && !effect))
				Debug.LogError("Music and effect it's true or false. GameObject: " + this.gameObject);

			this.sound = this.GetComponent<AudioSource>();

			if(music)
				updateVolume = new UpdateVolume(this.VolumeMusic);
			else
				updateVolume =new UpdateVolume(this.VolumeEffects);

		}

		// Use this for initialization
		void Update () {
			updateVolume();
		}

		private void VolumeMusic(){
			this.sound.volume = this.defaultVolume * PlayerPrefs.GetFloat(PlayerPrefsName.SoundMusic) * PlayerPrefs.GetFloat(PlayerPrefsName.SoundGeneral);
		}
		private void VolumeEffects(){
			this.sound.volume = this.defaultVolume * PlayerPrefs.GetFloat(PlayerPrefsName.SoundEffects) * PlayerPrefs.GetFloat(PlayerPrefsName.SoundGeneral);
		}
	}
}