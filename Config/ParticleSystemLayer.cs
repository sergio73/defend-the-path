﻿using UnityEngine;
using System.Collections;

namespace DefendThePath{
	public class ParticleSystemLayer : MonoBehaviour {
		[Range(-100,100)]
		public int layer = 0;
		
		void Awake() {
			particleSystem.renderer.sortingOrder = layer;
		}
	}
}