namespace DefendThePath{
	class PlayerPrefsName{
		public static readonly string SoundGeneral = "SoundGeneral";
		public static readonly string SoundMusic = "SoundMusic";
		public static readonly string SoundEffects = "SoundEffects";

		public static readonly string Shots = "Shots";
		public static readonly string EnemyDead = "EnemyDead";
		public static readonly string Score = "Score";
		public static readonly string Money = "Money";
		public static readonly string MoneySpent = "MoneySpent";
		public static readonly string Wins = "Wins";
		public static readonly string Loses = "Loses";
	}
}