using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

namespace DefendThePath{
	public class Spawner : MonoBehaviour{
		//Spawn
		public List<GameObject> enemiesToSpawn = new List<GameObject>();
		public float intervalSpawn = 2;
		private float intervalSpawnTime;

		public Transform spawnPosition;

		//Path
		public int nodesNumber;

		private PathSystem path;
		private static int enemiesToDie;

		void Awake(){
			this.CalculateMaxScore();

			enemiesToDie = this.enemiesToSpawn.Count;
			this.path = new PathSystem(this.nodesNumber);
			this.path.NodesCollect(); 

			this.style = new GUIStyle();
			this.style.fontSize = 64;
			this.style.normal.textColor = Color.white;
			this.style.alignment = TextAnchor.MiddleCenter;

			StartCoroutine(Countdown(5));
		}

		public Vector3 GetNextNode(int node){
			return path.GetNodePosition(node - 1);
		}
		public int GetNodesQty(){
			return this.nodesNumber;
		}
		void Update(){
			if(this.countdown != 0){
				return;
			}

			if(Grid.editorModeStatic)
				return;

			if((Time.time - intervalSpawnTime)>intervalSpawn){
				intervalSpawnTime = Time.time;
				SpawnEnemy();
			}
		}
		int countdown = 0;
		GUIStyle style;
		private IEnumerator Countdown(int s){
			for(int i = 0; i < s; i++){
				this.countdown = s - i;
				yield return new WaitForSeconds(1);
			}

			this.countdown = 0;
		}

		void OnGUI(){
			if(this.countdown != 0)
				GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 100, 100, 200), this.countdown.ToString(), this.style);
		}
		private void SpawnEnemy(){
			if(enemiesToSpawn.Count == 0)
				return;
			Instantiate(enemiesToSpawn[0], spawnPosition.position, spawnPosition.rotation);
			enemiesToSpawn.RemoveAt(0);
		}

		public static void EnemyDead(){
			enemiesToDie -= 1;

			if(enemiesToDie <= 0){
				HUD.MapEnd(true);
			}
		}

		[HideInInspector()]
		public int maxScore = 0;
		private void CalculateMaxScore(){
			foreach(GameObject enemy in enemiesToSpawn){
				maxScore += enemy.GetComponent<EnemyUnit>().score;
			}
		}
	}
}

