using UnityEngine;
using System;
using System.Collections.Generic;

namespace DefendThePath{
	public class PathSystem{
		private List<Transform> nodes = new List<Transform>();
		int numberNodes;

		public PathSystem(int numberNodes){
			this.numberNodes = numberNodes;
		}

		public void NodesCollect(){
			int i = 1;
			try{
				while(i <= this.numberNodes){
					this.nodes.Add(GameObject.Find("Node_" + i).transform);
					i++;
				}
			}catch(NullReferenceException ex){
				Debug.LogError("Node number: " + i + " does not exist");
				Debug.LogException(ex);
			}
		}

		public Vector3 GetNodePosition(int node){
			return this.nodes[node].position;
		}
	}
}

